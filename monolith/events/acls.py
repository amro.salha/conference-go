import requests
from .keys import PEXEL_KEY, OPEN_WEATHER_KEY
import json


def get_location_photo(city, state):
    headers = {"Authorization": PEXEL_KEY}

    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    resp = requests.get(url, headers=headers)

    return resp.json()["photos"][0]["url"]


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_KEY,
    }
    lat_url = "http://api.openweathermap.org/geo/1.0/direct"
    lat_resp = requests.get(lat_url, params=params)
    content = lat_resp.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    resp = requests.get(url, params=params)
    json_resp = resp.json()

    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]
    except (KeyError, IndexError):
        return None

    return {"temp": temp, "description": desc}

    # url = f"https://api.openweathermap.org/data/2.5/weather?{lat_resp['lat']}&{lat_resp['lon']}&appid={OPEN_WEATHER_KEY}"

    # resp = requests.get(url, headers=headers)
    # temp = resp.json()['main']['temp']
    # desc = resp.json()['weather']['description']
    # return {"temp" : temp, "desc" : desc}

    # ?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_KEY}"
