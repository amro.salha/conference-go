import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit'))
channel = connection.channel()

channel.queue_declare(queue='conferences')

def process_message(ch, method, properties, body):
    data = json.loads(body)
    print(f"Recieved {data}")

channel.basic_consume(queue='conferences',
                      auto_ack=True,
                      on_message_callback=process_message)

print(' [*] Waiting for conferences. To exit press CTRL+C')
channel.start_consuming()
